# HAML Spike

My project to learn HAML, specifically how to write cleaner partials and layouts.

## Setup

This app requires [Ruby 2.6](https://www.ruby-lang.org/en/) and the [bundler gem](https://bundler.io/man/bundle-gem.1.html).

Install the app's dependencies:
```bash
bundle install
```

## Development

To start the application:
```bash
rails s
```

View http://localhost:3000 in your browser!

## Preview

![preview](https://gitlab.com/jiaan/haml-spike/raw/master/demo.png)
